import random
import time
from selenium import webdriver
from selenium.webdriver import ActionChains, DesiredCapabilities


def get_track(offset):
    """
    通过偏移总量,模拟操作
    :param offset:
    :return:
    """
    # 步伐
    track = list()
    # 当前位移
    current = 0
    # 中间点 ,切换加速度
    mid = offset * 0.5
    t = 0.5
    v = 0
    while current < offset:
        a = 1.3
        v0 = v
        v = v0 + a * t
        move = v0 * t + 1 / 2 * a * (t ** 2)
        current += move
        track.append(round(move))
    return track


dcap = dict(DesiredCapabilities.PHANTOMJS)
dcap["phantomjs.page.settings.userAgent"] = (
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36")
    # "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
# 实例化一个启动参数对象
chrome_options = webdriver.ChromeOptions()
# 设置浏览器窗口大小
chrome_options.add_argument("--proxy-server=http://127.0.0.1:9000")
chrome_options.add_experimental_option('excludeSwitches', ['enable-automation'])  # 去除window.navigator.webdrive 为true
chrome_options.add_argument('--window-size=1366,768')
driver = webdriver.Chrome(desired_capabilities=dcap, options=chrome_options)
driver.get("http://e.waimai.meituan.com/logon")
# driver.execute_script("window.navigator.webdriver=false")
time.sleep(2)
iframe = driver.find_element_by_xpath('//*[@id="wm-logon"]/div[2]/iframe')
driver.switch_to.frame(iframe)
try:
    driver.find_element_by_id("login").send_keys("yrfh564341")
    
    driver.find_element_by_id("password").send_keys("yumg56")
    
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="login-form"]/button').click()
except:
    driver.close()
time.sleep(6)



track = get_track(200)
while True:
    button = driver.find_element_by_xpath('//*[@id="yodaBox"]')
    ActionChains(driver).click_and_hold(button).perform()
    for i in track:
        print(i)
        ActionChains(driver).move_by_offset(xoffset=i, yoffset=random.randint(1, 3)).perform()
    time.sleep(1)
    ActionChains(driver).release().perform()
    # driver.execute_script(
    #     "delete(window.navigator.webdriver);")
    time.sleep(10)
driver.close()