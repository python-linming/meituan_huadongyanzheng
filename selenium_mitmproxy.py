#!/home/cronuser/.virtualenvs/crawler/bin/python3
# coding:utf-8
"""=========================
@Project->File: meituan -> selenium_mitmproxy.py
@Describe:
@Author:
@Date: 9/19/19 4:28 PM
========================="""

from mitmproxy import ctx


def response(flow):
    """修改应答数据
    """
    INJECT_TEXT = 'Object.defineProperties(navigator,{webdriver:{get:() => false}});'  # js执行文件
    # if 'rohr.min.js' in flow.request.url:
    
    if '/js/yoda.' or 'rohr.min.js' or 'js/slider.' in flow.request.url:
        for webdriver_key in ['webdriver', '__driver_evaluate', '__webdriver_evaluate', '__selenium_evaluate',
                              '__fxdriver_evaluate', '__driver_unwrapped', '__webdriver_unwrapped',
                              '__selenium_unwrapped', '__fxdriver_unwrapped', '_Selenium_IDE_Recorder', '_selenium',
                              'calledSelenium', '_WEBDRIVER_ELEM_CACHE', 'ChromeDriverw', 'driver-evaluate',
                              'webdriver-evaluate', 'selenium-evaluate', 'webdriverCommand',
                              'webdriver-evaluate-response', '__webdriverFunc', '__webdriver_script_fn',
                              '__$webdriverAsyncExecutor', '__lastWatirAlert', '__lastWatirConfirm',
                              '__lastWatirPrompt', '$chrome_asyncScriptInfo', '$cdc_asdjflasutopfhvcZLmcfl_']:
        # # 屏蔽selenium检测
        # for webdriver_key in ['webdriver', '__driver_evaluate', '__webdriver_evaluate', '__selenium_evaluate',
        #                       '__fxdriver_evaluate', '__driver_unwrapped', '__webdriver_unwrapped',
        #                       '__selenium_unwrapped', '__fxdriver_unwrapped', '_Selenium_IDE_Recorder', '_selenium',
        #                       'calledSelenium', '_WEBDRIVER_ELEM_CACHE', 'ChromeDriverw', 'driver-evaluate',
        #                       'webdriver-evaluate', 'selenium-evaluate', 'webdriverCommand',
        #                       'webdriver-evaluate-response', '__webdriverFunc', '__webdriver_script_fn',
        #                       '__$webdriverAsyncExecutor', '__lastWatirAlert', '__lastWatirConfirm',
        #                       '__lastWatirPrompt', '$chrome_asyncScriptInfo', '$cdc_asdjflasutopfhvcZLmcfl_']:
            # for webdriver_key in ["webdriver", "__driver_evaluate", "__webdriver_evaluate", "__selenium_evaluate", "__fxdriver_evaluate","__driver_unwrapped", "__webdriver_unwrapped", "__selenium_unwrapped", "__fxdriver_unwrapped"]:
            ctx.log.info('Remove "{}" from {}.'.format(webdriver_key, flow.request.url))
            flow.response.text = flow.response.text.replace('"{}"'.format(webdriver_key), '"NO-SUCH-ATTR"')
        flow.response.text = flow.response.text.replace('t.webdriver', 'false')
        flow.response.text = flow.response.text.replace('ChromeDriver', '')
        # 屏蔽selenium检测
        # flow.response.text = flow.response.text + INJECT_TEXT
    # if "/rohr.min.js" in flow.request.url:
    #     flow.response.text = INJECT_TEXT + flow.response.text
    #     print('注入成功')